import 'dart:convert';

import 'package:twitchcorn/model/all_featured_stream.dart';
import 'package:twitchcorn/model/all_game.dart';
import 'package:twitchcorn/model/all_stream.dart';
import 'package:http/http.dart';

class TwitchService {
  static const _CLIENT_ID = 'l1dbeney812igyl37f7kh96j8z5q3n';
  static const _STREAM_URL = 'https://api.twitch.tv/kraken/streams';
  static const _FEATURED_STREAM_URL = '$_STREAM_URL/featured';
  static const _ALL_STREAM_URL = '$_STREAM_URL/?limit=100';
  static const _ALL_TOP_GAME_URL = 'https://api.twitch.tv/kraken/games/top/?limit=100';

  Client http = Client();

  Map<String, String> headers = {
    'Accept': 'application/vnd.twitchtv.v5+json',
    'Client-ID': '$_CLIENT_ID',
  };

  Future<Response> getFromUrl(String url) async {
    Response response = await http.get(url, headers: headers);
    return response;
  }

  Future<List<FeaturedStream>> fetchAllFeaturedStream() async {
    Response response = await getFromUrl(_FEATURED_STREAM_URL);

    if (response.statusCode == 200) {
      return AllFeaturedStream.fromJson(json.decode(response.body)).featured;
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<List<TwitchStream>> fetchAllStream() async {
    Response response = await getFromUrl(_ALL_STREAM_URL);

    if (response.statusCode == 200) {
      return AllStream.fromJson(json.decode(response.body)).streams;
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<List<TwitchGame>> fetchAllTopGame() async {
    Response response = await getFromUrl(_ALL_TOP_GAME_URL);
    if (response.statusCode == 200) {
      AllTopGame allTopGame = AllTopGame.fromJson(json.decode(response.body));
      return allTopGame.top;
    } else {
      throw Exception('Failed to load');
    }
  }

  Future<List<TwitchStream>> fetchAllStreamFromGame(String gameName) async {
    Response response = await getFromUrl('$_STREAM_URL/?game=$gameName&limit=100');
    if (response.statusCode == 200) {
      AllStream allStreamFromGame =
          AllStream.fromJson(json.decode(response.body));
      return allStreamFromGame.streams;
    } else {
      throw Exception('Failed to load');
    }
  }
}
