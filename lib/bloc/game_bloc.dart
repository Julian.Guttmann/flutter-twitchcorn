import 'dart:async';

import 'package:twitchcorn/bloc/utils/bloc_base.dart';
import 'package:twitchcorn/model/all_game.dart';
import 'package:twitchcorn/model/all_stream.dart';
import 'package:twitchcorn/services/twitch_service.dart';

class GameBloc extends BlocBase {
  final TwitchService twitchService = TwitchService();

  final _allTopGame = StreamController<List<TwitchGame>>.broadcast();
  final _allStreamFromGame = StreamController<List<TwitchStream>>();

  Stream<List<TwitchGame>> get allTopGame => _allTopGame.stream;
  Stream<List<TwitchStream>> get allStreamFromGame => _allStreamFromGame.stream;

  void fetchAllTopGame() async {
    final List<TwitchGame> allTopGame = await twitchService.fetchAllTopGame();
    _allTopGame.sink.add(allTopGame);
  }

  void fetchAllStreamFromGame(String gameName) async {
    final List<TwitchStream> allStreamFromGame =
        await twitchService.fetchAllStreamFromGame(gameName);
    _allStreamFromGame.sink.add(allStreamFromGame);
  }

  @override
  void dispose() {
    _allTopGame.close();
    _allStreamFromGame.close();
  }
}
