import 'dart:async';

import 'package:twitchcorn/bloc/utils/bloc_base.dart';
import 'package:twitchcorn/model/all_featured_stream.dart';
import 'package:twitchcorn/model/all_stream.dart';
import 'package:twitchcorn/services/twitch_service.dart';

class StreamBloc extends BlocBase {
  final TwitchService streamService = TwitchService();

  final _allFeaturedStream = StreamController<List<FeaturedStream>>.broadcast();
  final _allStream = StreamController<List<TwitchStream>>.broadcast();

  Stream<List<FeaturedStream>> get allFeaturedStream =>
      _allFeaturedStream.stream;
  Stream<List<TwitchStream>> get allStream => _allStream.stream;

  void fetchAllFeaturedStream() async {
    final List<FeaturedStream> allFeaturedChannel =
        await streamService.fetchAllFeaturedStream();

    _allFeaturedStream.sink.add(allFeaturedChannel);
  }

  void fetchAllStream() async {
    final List<TwitchStream> allStream = await streamService.fetchAllStream();

    _allStream.sink.add(allStream);
  }

  @override
  void dispose() {
    _allFeaturedStream.close();
    _allStream.close();
  }
}
