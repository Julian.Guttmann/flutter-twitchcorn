import 'package:flutter/material.dart';
import 'package:twitchcorn/model/all_stream.dart';
import 'package:twitchcorn/ui/pages/stream_page.dart';

class StreamGridUtils {
  static ListView buildStreamListView(List<TwitchStream> allTwitchStream) {
    return ListView.builder(
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: allTwitchStream.length,
      itemBuilder: (context, index) =>
          buildStreamListItem(allTwitchStream[index], context),
    );
  }

  static Widget buildStreamListItem(
    TwitchStream twitchStream,
    BuildContext context,
  ) {
    return Column(
      children: <Widget>[
        ListTile(
          onTap: () => Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) =>
                  StreamPage(twitchStream.channel.name),
            ),
          ),
          contentPadding: const EdgeInsets.fromLTRB(
            8.0,
            2.0,
            2.0,
            2.0,
          ),
          leading: Stack(
            children: <Widget>[
              Container(
                child: Card(
                  elevation: 4.0,
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Image(
                    fit: BoxFit.fitHeight,
                    image: NetworkImage(twitchStream.preview.large),
                  ),
                ),
                width: 80,
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Transform(
                  child: Chip(
                    shadowColor: Colors.teal,
                    backgroundColor: Colors.redAccent,
                    label: Text(
                      '${twitchStream.viewers.toString()}',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                  transform: new Matrix4.identity()..scale(0.5),
                ),
              ),
            ],
          ),
          title: Text(
            twitchStream.channel.status,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '${twitchStream.channel.name}',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: Colors.blueAccent,
                ),
              ),
              Text(
                '${twitchStream.game}',
                style: TextStyle(
                  color: Colors.blueGrey,
                ),
              ),
            ],
          ),
        ),
        Divider()
      ],
    );
  }
}
