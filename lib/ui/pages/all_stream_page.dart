import 'package:backdrop/backdrop.dart';
import 'package:flutter/material.dart';
import 'package:twitchcorn/bloc/stream_bloc.dart';
import 'package:twitchcorn/bloc/utils/bloc_provider.dart';
import 'package:twitchcorn/model/all_stream.dart';
import 'package:twitchcorn/ui/widgets/stream_grid_utils.dart';

class AllStreamPage extends StatelessWidget {
  AllStreamPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final StreamBloc streamBloc = BlocProvider.of<StreamBloc>(context);
    streamBloc.fetchAllStream();

    return Scaffold(
      body: BackdropScaffold(
        title: Text("Browse Streams"),
        frontLayer: _buildStreamList(streamBloc),
        backLayer: _buildFilterList(streamBloc),
        iconPosition: BackdropIconPosition.none,
        actions: <Widget>[
          BackdropToggleButton(
            icon: AnimatedIcons.list_view,
          ),
        ],
      ),
    );
  }

  Widget _buildFilterList(StreamBloc streamBloc) {
    return Center(
      child: Text("Set your filters here"),
    );
  }

  Widget _buildStreamList(StreamBloc streamBloc) {
    return Center(
      child: Container(
        child: StreamBuilder<List<TwitchStream>>(
          stream: streamBloc.allStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return StreamGridUtils.buildStreamListView(
                snapshot.data,
              );
            } else if (snapshot.hasError) {
              return Text(
                snapshot.error.toString(),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}
