import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twitchcorn/bloc/game_bloc.dart';
import 'package:twitchcorn/bloc/utils/bloc_provider.dart';
import 'package:twitchcorn/model/all_game.dart';
import 'package:twitchcorn/model/all_stream.dart';
import 'package:twitchcorn/ui/widgets/stream_grid_utils.dart';

class GamePage extends StatelessWidget {
  final Game _game;

  GamePage(this._game);

  @override
  Widget build(BuildContext context) {
    GameBloc gameBloc = BlocProvider.of<GameBloc>(context);
    gameBloc.fetchAllStreamFromGame(_game.name);
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Hero(
            tag: _game.name,
            child: Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Image(
                fit: BoxFit.fitHeight,
                image: NetworkImage(_game.box.large),
              ),
            ),
          ),
        ],
        title: Text(_game.name),
      ),
      body: Center(
        child: StreamBuilder<List<TwitchStream>>(
          stream: gameBloc.allStreamFromGame,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return StreamGridUtils.buildStreamListView(
                snapshot.data,
              );
            } else if (snapshot.hasError) {
              return Text(
                snapshot.error.toString(),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}
