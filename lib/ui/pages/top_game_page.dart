import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:twitchcorn/bloc/game_bloc.dart';
import 'package:twitchcorn/bloc/utils/bloc_provider.dart';
import 'package:twitchcorn/model/all_game.dart';
import 'package:twitchcorn/ui/pages/game_page.dart';

class TopGamePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final GameBloc gameBloc = BlocProvider.of<GameBloc>(context);
    gameBloc.fetchAllTopGame();

    return Scaffold(
      body: StreamBuilder<List<TwitchGame>>(
        stream: gameBloc.allTopGame,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 68 / 95,
              ),
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) =>
                  _buildGameGridItem(snapshot.data[index], context),
            );
          } else if (snapshot.hasError) {
            return Text(
              snapshot.error.toString(),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildGameGridItem(TwitchGame twitchGame, BuildContext context) {
    return Tooltip(
      message: twitchGame.game.name,
      child: InkWell(
        enableFeedback: true,
        splashColor: Colors.purple,
        onTap: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => BlocProvider<GameBloc>(
              bloc: GameBloc(),
              child: GamePage(twitchGame.game),
            ),
          ),
        ),
        child: Hero(
          tag: twitchGame.game.name,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              elevation: 8.0,
              child: Image(
                fit: BoxFit.fill,
                image: NetworkImage(twitchGame.game.box.large),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
