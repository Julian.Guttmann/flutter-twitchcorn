import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class StreamPage extends StatelessWidget {
  final String _channelName;

  const StreamPage(this._channelName);

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;

    bool portrait = orientation == Orientation.portrait;

    WebView player = WebView(
      initialUrl:
          'https://player.twitch.tv/?channel=$_channelName&autoplay=true&muted=false',
      javascriptMode: JavascriptMode.unrestricted,
    );

    Widget playerAndChat = Column(
      children: <Widget>[
        Expanded(flex: 1, child: player),
        Expanded(
          flex: 2,
          child: WebView(
            initialUrl: 'https://www.twitch.tv/embed/$_channelName/chat',
            javascriptMode: JavascriptMode.unrestricted,
          ),
        ),
      ],
    );

    if (portrait) {
      return SafeArea(
        child: Scaffold(
          body: Center(
            child: playerAndChat,
          ),
        ),
      );
    } else {
      return SafeArea(
        child: Scaffold(
          body: Center(
            child: player,
          ),
        ),
      );
    }
  }
}
