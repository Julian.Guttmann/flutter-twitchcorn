import 'package:flutter/material.dart';
import 'package:twitchcorn/bloc/stream_bloc.dart';
import 'package:twitchcorn/bloc/utils/bloc_provider.dart';
import 'package:twitchcorn/model/all_featured_stream.dart';
import 'package:twitchcorn/model/all_stream.dart';
import 'package:twitchcorn/ui/widgets/stream_grid_utils.dart';

class AllFeaturedStreamPage extends StatelessWidget {
  AllFeaturedStreamPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final StreamBloc streamBloc = BlocProvider.of<StreamBloc>(context);
    streamBloc.fetchAllFeaturedStream();

    return Scaffold(
      body: Container(
        child: StreamBuilder<List<FeaturedStream>>(
          stream: streamBloc.allFeaturedStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return StreamGridUtils.buildStreamListView(
                getAllTwitchStreamFromFeatured(snapshot.data),
              );
            } else if (snapshot.hasError) {
              return Text(
                snapshot.error.toString(),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }

  List<TwitchStream> getAllTwitchStreamFromFeatured(
      List<FeaturedStream> allFaturedStream) {
    return allFaturedStream.map((f) => f.stream).toList();
  }
}
