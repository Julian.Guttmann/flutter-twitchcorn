import 'package:flutter/material.dart';
import 'package:twitchcorn/bloc/game_bloc.dart';
import 'package:twitchcorn/bloc/stream_bloc.dart';
import 'package:twitchcorn/bloc/utils/bloc_provider.dart';
import 'package:twitchcorn/ui/pages/all_stream_page.dart';
import 'package:twitchcorn/ui/pages/featured_page.dart';
import 'package:twitchcorn/ui/pages/top_game_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

enum Tabs { featured, browse, game, profile }

class _MyAppState extends State<MyApp> {
  int _currentPageIndex = 0;

  void _incrementTab(index) {
    setState(() {
      _currentPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    var allPages = [
      _buildPage(context, Tabs.featured),
      _buildPage(context, Tabs.browse),
      _buildPage(context, Tabs.game),
      _buildPage(context, Tabs.profile),
    ];

    var allBottomNavigationBarItem = [
      _buildBottomNavigationBarItem(
        Icons.whatshot,
        'Featured',
      ),
      _buildBottomNavigationBarItem(
        Icons.public,
        'Browse',
      ),
      _buildBottomNavigationBarItem(
        Icons.videogame_asset,
        'Games',
      ),
      _buildBottomNavigationBarItem(
        Icons.person,
        'Profile',
      ),
    ];

    return MaterialApp(
      title: 'Twitchcorn',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        accentColor: Colors.pinkAccent,
      ),
      home: Scaffold(
        body: allPages.elementAt(_currentPageIndex),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentPageIndex,
          type: BottomNavigationBarType.shifting,
          items: allBottomNavigationBarItem,
          onTap: (index) {
            _incrementTab(index);
          },
        ),
      ),
    );
  }

  BottomNavigationBarItem _buildBottomNavigationBarItem(
    IconData icon,
    String title,
  ) {
    return BottomNavigationBarItem(
      icon: Icon(icon, color: Theme.of(context).accentColor),
      title: Text(
        title,
        style: TextStyle(color: Theme.of(context).accentColor),
      ),
    );
  }

  Widget _buildPage(BuildContext context, Tabs tab) {
    switch (tab) {
      case Tabs.featured:
        return BlocProvider<StreamBloc>(
          bloc: StreamBloc(),
          child: AllFeaturedStreamPage(),
        );
        break;

      case Tabs.browse:
        return BlocProvider<StreamBloc>(
          bloc: StreamBloc(),
          child: AllStreamPage(),
        );
        break;
      case Tabs.game:
        return BlocProvider<GameBloc>(
          bloc: GameBloc(),
          child: TopGamePage(),
        );
        break;
      default:
        return Center(
          child: CircularProgressIndicator(),
        );
    }
  }
}
