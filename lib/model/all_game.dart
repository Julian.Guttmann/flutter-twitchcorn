class AllTopGame {
  int iTotal;
  List<TwitchGame> top;

  AllTopGame({this.iTotal, this.top});

  AllTopGame.fromJson(Map<String, dynamic> json) {
    iTotal = json['_total'];
    if (json['top'] != null) {
      top = new List<TwitchGame>();
      json['top'].forEach((v) {
        top.add(new TwitchGame.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_total'] = this.iTotal;
    if (this.top != null) {
      data['top'] = this.top.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TwitchGame {
  int channels;
  int viewers;
  Game game;

  TwitchGame({this.channels, this.viewers, this.game});

  TwitchGame.fromJson(Map<String, dynamic> json) {
    channels = json['channels'];
    viewers = json['viewers'];
    game = json['game'] != null ? new Game.fromJson(json['game']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['channels'] = this.channels;
    data['viewers'] = this.viewers;
    if (this.game != null) {
      data['game'] = this.game.toJson();
    }
    return data;
  }
}

class Game {
  int iId;
  Box box;
  int giantbombId;
  Logo logo;
  String name;
  int popularity;

  Game(
      {this.iId,
      this.box,
      this.giantbombId,
      this.logo,
      this.name,
      this.popularity});

  Game.fromJson(Map<String, dynamic> json) {
    iId = json['_id'];
    box = json['box'] != null ? new Box.fromJson(json['box']) : null;
    giantbombId = json['giantbomb_id'];
    logo = json['logo'] != null ? new Logo.fromJson(json['logo']) : null;
    name = json['name'];
    popularity = json['popularity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.iId;
    if (this.box != null) {
      data['box'] = this.box.toJson();
    }
    data['giantbomb_id'] = this.giantbombId;
    if (this.logo != null) {
      data['logo'] = this.logo.toJson();
    }
    data['name'] = this.name;
    data['popularity'] = this.popularity;
    return data;
  }
}

class Box {
  String large;
  String medium;
  String small;
  String template;

  Box({this.large, this.medium, this.small, this.template});

  Box.fromJson(Map<String, dynamic> json) {
    large = json['large'];
    medium = json['medium'];
    small = json['small'];
    template = json['template'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['large'] = this.large;
    data['medium'] = this.medium;
    data['small'] = this.small;
    data['template'] = this.template;
    return data;
  }
}

class Logo {
  String large;
  String medium;
  String small;
  String template;

  Logo({this.large, this.medium, this.small, this.template});

  Logo.fromJson(Map<String, dynamic> json) {
    large = json['large'];
    medium = json['medium'];
    small = json['small'];
    template = json['template'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['large'] = this.large;
    data['medium'] = this.medium;
    data['small'] = this.small;
    data['template'] = this.template;
    return data;
  }
}