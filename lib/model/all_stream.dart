class AllStream {
  int iTotal;
  List<TwitchStream> streams;

  AllStream({this.iTotal, this.streams});

  AllStream.fromJson(Map<String, dynamic> json) {
    iTotal = json['_total'];
    if (json['streams'] != null) {
      streams = new List<TwitchStream>();
      json['streams'].forEach((v) {
        streams.add(new TwitchStream.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_total'] = this.iTotal;
    if (this.streams != null) {
      data['streams'] = this.streams.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TwitchStream {
  int iId;
  String game;
  String broadcastPlatform;
  int viewers;
  int videoHeight;
  var averageFps;
  int delay;
  String createdAt;
  bool isPlaylist;
  String streamType;
  Preview preview;
  Channel channel;

  TwitchStream(
      {this.iId,
      this.game,
      this.broadcastPlatform,
      this.viewers,
      this.videoHeight,
      this.averageFps,
      this.delay,
      this.createdAt,
      this.isPlaylist,
      this.streamType,
      this.preview,
      this.channel});

  TwitchStream.fromJson(Map<String, dynamic> json) {
    iId = json['_id'];
    game = json['game'];
    broadcastPlatform = json['broadcast_platform'];
    viewers = json['viewers'];
    videoHeight = json['video_height'];
    averageFps = json['average_fps'];
    delay = json['delay'];
    createdAt = json['created_at'];
    isPlaylist = json['is_playlist'];
    streamType = json['stream_type'];
    preview =
        json['preview'] != null ? new Preview.fromJson(json['preview']) : null;
    channel =
        json['channel'] != null ? new Channel.fromJson(json['channel']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.iId;
    data['game'] = this.game;
    data['broadcast_platform'] = this.broadcastPlatform;
    data['viewers'] = this.viewers;
    data['video_height'] = this.videoHeight;
    data['average_fps'] = this.averageFps;
    data['delay'] = this.delay;
    data['created_at'] = this.createdAt;
    data['is_playlist'] = this.isPlaylist;
    data['stream_type'] = this.streamType;
    if (this.preview != null) {
      data['preview'] = this.preview.toJson();
    }
    if (this.channel != null) {
      data['channel'] = this.channel.toJson();
    }
    return data;
  }
}

class Preview {
  String small;
  String medium;
  String large;
  String template;

  Preview({this.small, this.medium, this.large, this.template});

  Preview.fromJson(Map<String, dynamic> json) {
    small = json['small'];
    medium = json['medium'];
    large = json['large'];
    template = json['template'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['small'] = this.small;
    data['medium'] = this.medium;
    data['large'] = this.large;
    data['template'] = this.template;
    return data;
  }
}

class Channel {
  bool mature;
  String status;
  String broadcasterLanguage;
  String broadcasterSoftware;
  String displayName;
  String game;
  String language;
  int iId;
  String name;
  String createdAt;
  String updatedAt;
  bool partner;
  String logo;
  String videoBanner;
  String profileBanner;
  String profileBannerBackgroundColor;
  String url;
  int views;
  int followers;
  String broadcasterType;
  String description;
  bool privateVideo;
  bool privacyOptionsEnabled;

  Channel(
      {this.mature,
      this.status,
      this.broadcasterLanguage,
      this.broadcasterSoftware,
      this.displayName,
      this.game,
      this.language,
      this.iId,
      this.name,
      this.createdAt,
      this.updatedAt,
      this.partner,
      this.logo,
      this.videoBanner,
      this.profileBanner,
      this.profileBannerBackgroundColor,
      this.url,
      this.views,
      this.followers,
      this.broadcasterType,
      this.description,
      this.privateVideo,
      this.privacyOptionsEnabled});

  Channel.fromJson(Map<String, dynamic> json) {
    mature = json['mature'];
    status = json['status'];
    broadcasterLanguage = json['broadcaster_language'];
    broadcasterSoftware = json['broadcaster_software'];
    displayName = json['display_name'];
    game = json['game'];
    language = json['language'];
    iId = json['_id'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    partner = json['partner'];
    logo = json['logo'];
    videoBanner = json['video_banner'];
    profileBanner = json['profile_banner'];
    profileBannerBackgroundColor = json['profile_banner_background_color'];
    url = json['url'];
    views = json['views'];
    followers = json['followers'];
    broadcasterType = json['broadcaster_type'];
    description = json['description'];
    privateVideo = json['private_video'];
    privacyOptionsEnabled = json['privacy_options_enabled'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['mature'] = this.mature;
    data['status'] = this.status;
    data['broadcaster_language'] = this.broadcasterLanguage;
    data['broadcaster_software'] = this.broadcasterSoftware;
    data['display_name'] = this.displayName;
    data['game'] = this.game;
    data['language'] = this.language;
    data['_id'] = this.iId;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['partner'] = this.partner;
    data['logo'] = this.logo;
    data['video_banner'] = this.videoBanner;
    data['profile_banner'] = this.profileBanner;
    data['profile_banner_background_color'] = this.profileBannerBackgroundColor;
    data['url'] = this.url;
    data['views'] = this.views;
    data['followers'] = this.followers;
    data['broadcaster_type'] = this.broadcasterType;
    data['description'] = this.description;
    data['private_video'] = this.privateVideo;
    data['privacy_options_enabled'] = this.privacyOptionsEnabled;
    return data;
  }
}
